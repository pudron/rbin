# rbin

#### 介绍
读取文件并转化成C语言十六进制数组


#### 安装教程

* `make`

#### 使用说明

* `rbin`输出参数信息
* `rbin [要转化的文件] [C语言变量名] [输出文件名] [线程数(若不指定，则默认为3)]`
* 示例：`rbin demo.exe var_demo demo.h`

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

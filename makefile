BUILD ?= RELEASE
CFLAGS=
ifeq ($(BUILD),DEBUG)
CFLAGS+= -g
endif
rbin:rbin.c
	gcc -Wall -O2 $(CFLAGS) rbin.c -o rbin -lpthread

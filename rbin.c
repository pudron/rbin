#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<errno.h>
#include<pthread.h>
typedef struct{
	unsigned char*bin;
	int len;
	char*text;
	bool isShow;
	int id;
}Arg;
unsigned char*readFile(char*fileName,int*length){
	FILE*fp=fopen(fileName,"rb");
	if(fp==NULL){
		return NULL;
	}
	fseek(fp,0,SEEK_END);
	int len=ftell(fp);
	rewind(fp);
	unsigned char*bin=(unsigned char*)malloc(len);
	len=fread(bin,1,len,fp);
	fclose(fp);
	*length=len;
	return bin;
}
bool writeFile(char*fileName,char*text){
	FILE*fp=fopen(fileName,"wt");
	if(fp==NULL){
		return false;
	}
	fwrite(text,1,strlen(text),fp);
	fclose(fp);
	return true;
}
void*change(void*a){
	Arg arg=*((Arg*)a);
	char temp[64];
	for(int i=0;i<arg.len;i++){
		if(arg.isShow){
			printf("byte[%d]:%#X;",arg.id,arg.bin[i]);
		}
		sprintf(temp,",%#x",arg.bin[i]);
		strcat(arg.text,temp);
	}
	pthread_exit(NULL);
	return NULL;
}
char*binToText(unsigned char*bin,int len,char*varName,int threadCount,bool isShow){
	if(len<=0){
		return NULL;
	}
	if(threadCount>=len){
		threadCount=1;
	}
	pthread_t*ths=(pthread_t*)malloc(threadCount*sizeof(pthread_t));
	char*text=(char*)malloc(len*11+2*strlen(varName)+64);
	int section=len/threadCount;
	int offset=len%threadCount;
	Arg*args=(Arg*)malloc(threadCount*sizeof(Arg));
	for(int i=0;i<threadCount;i++){
		args[i]=(Arg){bin+i*section,section,(char*)malloc(section*11+1),isShow,i};
		if(i==threadCount-1){
			args[i].len+=offset;
		}
		args[i].text[0]='\0';
		if(pthread_create(&ths[i],NULL,change,&args[i])!=0){
			printf("create thread fail\n");
			return NULL;
		}
	}
	sprintf(text,"const int %s_length=%d;unsigned char %s[]=",varName,len,varName);
	for(int i=0;i<threadCount;i++){
		pthread_join(ths[i],NULL);
		if(i==0){
			args[i].text[0]='{';
		}
		strcat(text,args[i].text);
		free(args[i].text);
	}
	free(ths);
	strcat(text,"};");
	return text;
}
int main(int argc,char**argv){
	if(argc<4){
		printf("usage:rbin [input file name] [variable name] [output file name] [thread count]\nEnjoy!\n");
		return 0;
	}
	int threadCount=3;
	#ifdef DEBUG
	bool isShow=true;
	#else
	bool isShow=false;
	#endif
	if(argc==5){
		threadCount=atoi(argv[4]);
	}
	int len;
	unsigned char*bin=readFile(argv[1],&len);
	if(bin==NULL){
		printf("error:%s\n",strerror(errno));
		return 1;
	}
	char*text=binToText(bin,len,argv[2],threadCount,isShow);
	free(bin);
	if(text==NULL){
		printf("translate error\n");
		return 1;
	}
	if(!writeFile(argv[3],text)){
		printf("error:%s\n",strerror(errno));
		free(text);
		return 1;
	}
	printf("input: %d bytes,output: %I64d bytes\n",len,strlen(text));
	free(text);
	return 0;
}
